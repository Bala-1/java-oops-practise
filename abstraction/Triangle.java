package abstraction;

public class Triangle extends Shape {

	private double height;
	private double base;

	/**
	 * @param height
	 * @param base
	 */
	public Triangle(double height, double base) {
		this.height = height;
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
		return (base * height) / 2;
	}

	@Override
	public double calculateCircumference() {
		// TODO Auto-generated method stub
		return 0;
	}

}
