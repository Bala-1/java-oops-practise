package abstraction;

class Rectangle extends Shape {

	private double height;
	private double width;

	/**
	 * @param height
	 * @param width
	 */
	public Rectangle(double height, double width) {
		super();
		this.height = height;
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
		return height * width;
	}

	@Override
	public double calculateCircumference() {
		// TODO Auto-generated method stub
		return 2 * (height * width);
	}

}
