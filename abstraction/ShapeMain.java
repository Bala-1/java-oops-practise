package abstraction;

public class ShapeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Rectangle newRectangle= new Rectangle(2,4);
		
		System.out.println("area of the rectangle is : "+newRectangle.calculateArea());
		System.out.println("the circumference of the rectangle is : "+newRectangle.calculateCircumference());
		
		newRectangle.setHeight(8);
		System.out.println("area of the rectangle is : "+newRectangle.calculateArea());
		
		


	}

}
