package abstraction;

class Circle extends Shape {

	private double radius;

	/**
	 * @param radius
	 */
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}


	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub

		return 3.14 * radius * radius;
	}

	@Override
	public double calculateCircumference() {
		// TODO Auto-generated method stub
		return 2 * 3.14 * radius;
	}

	
}
