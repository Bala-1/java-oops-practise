package book;

public class BookStore {
	
	String bookId;
	String bookName;
	double bookPrice;
	String author;
	String publisher;

	public BookStore(String bookId, String bookName, double bookPrice, String author, String publisher) {
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookPrice = bookPrice;
		this.author = author;
		this.publisher = publisher;
	}
	
	public String toString() {
		return bookId+" "+bookName+" "+bookPrice+" "+author+" "+publisher;
	}
	
	

}
