package book;

import java.util.Comparator;

public class BooksSortByPublisher implements Comparator<BookStore> {

	@Override
	public int compare(BookStore book1, BookStore book2) {
		return book1.publisher.compareTo(book2.publisher);
	
		//return book1.publisher.compareToIgnoreCase(book2.publisher);
	}

}
