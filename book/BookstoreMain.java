package book;

import java.util.ArrayList;
import java.util.Collections;

public class BookstoreMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       BookStore book1 = new BookStore("ID-100", "english novel", 200 , "shakesphere","modern Library");
       BookStore book2 = new BookStore("ID-101", "Data Structure", 217.50 , "robert","Mc Graw");
       BookStore book3 = new BookStore("ID-102", "Dos Guide ", 175, "Nortorn","Mc Graw");
       BookStore book4 = new BookStore("ID-103", "COBOL ", 1000 , "Stern","John W");
       BookStore book5 = new BookStore("ID-104", "Basics for beginers", 250 , "Nortorn","modern Library");
       BookStore book6 = new BookStore("ID-105", "Computer Studies", 800 , "French","galgotia");
       
       ArrayList<BookStore> listOfBooks = new ArrayList<BookStore>();
       
       listOfBooks.add(book1);
       listOfBooks.add(book2);
       listOfBooks.add(book3);
       listOfBooks.add(book4);
       listOfBooks.add(book5);
       listOfBooks.add(book6);
       
       System.out.println("unsorted:");
       for (int i =0 ;i <listOfBooks.size(); i++ ) {
    	   System.out.println(listOfBooks.get(i));
       }
       
       Collections.sort(listOfBooks, new BooksSortByPublisher());
       
       System.out.println("sorted:");
       for (int i =0 ;i <listOfBooks.size(); i++ ) {
    	   System.out.println(listOfBooks.get(i));
       }

	}

}
