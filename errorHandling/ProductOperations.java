package errorHandling;

import java.util.ArrayList;
import java.util.Iterator;

public class ProductOperations {

	ArrayList<Product> listOfProducts = new ArrayList<Product>();

	/**
	 * 
	 * Method adds the products to the list
	 * 
	 **/

	public void addProducts(Product p) {

		listOfProducts.add(p);
	}

	/**
	 * 
	 * this method display the list of products
	 * 
	 **/

	public void displayListOfProducts() {

		Iterator<Product> product = listOfProducts.iterator();
		Product p =new Product();
		
			while (product.hasNext()) {
				p= product.next();
				//validProductCheck(product.next());
				System.out.println(p);
			}
		

		
	}

	public void displayValidListOfProducts() {

		Iterator<Product> product = listOfProducts.iterator();
		Product p =new Product();
		try {
			while (product.hasNext()) {
				p= product.next();
				validProductCheck(p);
				System.out.println(p);
			}
		} catch (Exception e) {
			System.out.println("error "+e+" in "+p);
			
			setValidList(p);

		}
	}

	public void setValidList(Product p) {
		if(listOfProducts.contains(p)) {
			listOfProducts.remove(p);
			displayValidListOfProducts();
		}
	}
	
	/**
	 * @throws Exception 
	 * 
	 * 
	 * **/
//	public void checkValidProducts() {
//		Iterator<Product> productToCheck = listOfProducts.iterator();
//		
//		while(productToCheck.hasNext()) {
//			if(productToCheck.next().getWeight() < 200) {
//				throw new Exception("Invalid product Exception: product weight must be greater than 200 kgs");
//			}
//		}

		public void validProductCheck(Product p) throws Exception {
			if(p.getWeight() < 200.0) {
			throw new Exception("Invalid product Exception: product weight must be greater than 200 kgs");

			}
	}

}
