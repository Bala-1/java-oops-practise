package errorHandling;

import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) {
		
		//ArrayList<Product> listOfProducts = new ArrayList<Product>();
		
		Product p1 = new Product(1001, 100, 250);
		Product p2 = new Product(1002, 250, 120);
		Product p3 = new Product(1003, 220, 120);
		Product p4 = new Product(1004, 260, 120);
		Product p5 = new Product(1005, 190, 120);
		Product p6 = new Product(1006, 200, 120);
		Product p7 = new Product(1007, 300, 120);
		Product p8 = new Product(1008, 150, 120);
		Product p9 = new Product(1009, 300, 120);
		Product p10 = new Product(1010, 230, 120);
		
		ProductOperations newOperation = new ProductOperations();
		
		newOperation.addProducts(p1);
		newOperation.addProducts(p2);
		newOperation.addProducts(p3);
		newOperation.addProducts(p4);
		newOperation.addProducts(p5);
		newOperation.addProducts(p6);
		newOperation.addProducts(p7);
		newOperation.addProducts(p8);
		newOperation.addProducts(p9);
		newOperation.addProducts(p10);

		System.out.println("original products");
		 newOperation.displayListOfProducts();
		
		
		
	}

}
