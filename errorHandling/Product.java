package errorHandling;

public class Product {
	private int productId;
	private double weight;
	private double price;
	
	public Product() {
		
	}
	
	

	public Product(int productId, double weight, double price) {
		super();
		this.productId = productId;
		this.weight = weight;
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", weight=" + weight + ", price=" + price + "]";
	}

}
