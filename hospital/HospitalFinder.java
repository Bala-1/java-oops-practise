package hospital;

import java.util.HashMap;
import java.util.List;

public class HospitalFinder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hospital h1 = new Hospital("Apolo",List.of("ENT","Ortho","Gastro"),"Charan","9876598765","Hyderabad");
        Hospital h2 = new Hospital("Care",List.of("ENT","Ortho","Cardiac"),"Rohit","9856756793","Vizag");
        Hospital h3 = new Hospital("Seven Hills",List.of("ENT","Gastro"),"Rahul","7895687635","Chennai");
        Hospital h4 = new Hospital("Q1",List.of("ENT","Ortho","Cardiac","pediatric"),"Kohli","769456234","Banglore");
        Hospital h5 = new Hospital("Vijaya",List.of("ENT","Ortho"),"Dhoni","9634526798","Kolkata");
        
        HospitalService service = new HospitalService();
        
        service.addHospital(h1);
        service.addHospital(h2);
        service.addHospital(h3);
        service.addHospital(h4);
        service.addHospital(h5);
        
        //service.displayAllHospitals();
        
//        Hospital res = service.getHospitalDetails(1001);
//        System.out.println(res);
        
        HashMap<Integer,String> res = service.getHospital();
        System.out.println(res);

	}

}
