package hospital;

import java.util.List;

public class Hospital {

	  private int hospitalCode;
	    static int codeGen = 1000;
	    private String hospitalName;
	    private List<String> listOfTreatments;
	    private String ContactPerson;
	    private String ContactNumber;
	    private String Location;
	    
	    public Hospital() {
	        this.hospitalCode = codeGen++;
	    }



	   public Hospital(String hospitalName, List<String> listOfTreatments, String contactPerson, String contactNumber,
	            String location) {
	        
	        this();
	        this.hospitalName = hospitalName;
	        this.listOfTreatments = listOfTreatments;
	        ContactPerson = contactPerson;
	        ContactNumber = contactNumber;
	        Location = location;
	    }



	   public int getHospitalCode() {
	        return hospitalCode;
	    }



	   public void setHospitalCode(int hospitalCode) {
	        this.hospitalCode = hospitalCode;
	    }



	   public String getHospitalName() {
	        return hospitalName;
	    }



	   public void setHospitalName(String hospitalName) {
	        this.hospitalName = hospitalName;
	    }



	   public List<String> getListOfTreatments() {
	        return listOfTreatments;
	    }



	   public void setListOfTreatments(List<String> listOfTreatments) {
	        this.listOfTreatments = listOfTreatments;
	    }



	   public String getContactPerson() {
	        return ContactPerson;
	    }



	   public void setContactPerson(String contactPerson) {
	        ContactPerson = contactPerson;
	    }



	   public String getContactNumber() {
	        return ContactNumber;
	    }



	   public void setContactNumber(String contactNumber) {
	        ContactNumber = contactNumber;
	    }



	   public String getLocation() {
	        return Location;
	    }



	   public void setLocation(String location) {
	        Location = location;
	    }



	   @Override
	    public String toString() {
	        return "Hospital [hospitalCode=" + hospitalCode + ", hospitalName=" + hospitalName + ", listOfTreatments="
	                + listOfTreatments + ", ContactPerson=" + ContactPerson + ", ContactNumber=" + ContactNumber
	                + ", Location=" + Location + "]";
	    }
	
}
