package hospital;

import java.util.*;



public class HospitalService {
    
    List<Hospital> hospitalList = new ArrayList<>();
    
    public int addHospital(Hospital hospital) {
        hospitalList.add(hospital);
        return hospital.getHospitalCode();
    }
    
    public Hospital getHospitalDetails(int code) {
        for(Hospital hospitalObj :hospitalList) {
            if(hospitalObj.getHospitalCode() == code)
                return hospitalObj;
        }
        return null;
    }
    
    public HashMap<Integer,String> getHospital() {
        HashMap<Integer,String> map = new HashMap<>();
        for(Hospital hospitalObj :hospitalList) {
            map.put(hospitalObj.getHospitalCode(), hospitalObj.getHospitalName());
        }
        return map;
    }
    
    public void displayAllHospitals() {
        for(Hospital hospitalObj :hospitalList) {
            System.out.println(hospitalObj);
        }
    }
    
}