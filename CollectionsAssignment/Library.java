package CollectionsAssignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Library {

	public static void displayMenu() {
		System.out.println("Please select the option from the menu");
		System.out.println("1-Add the book");
		System.out.println("2-Update the book details");
		System.out.println("3-Search the book");
		System.out.println("4-get the books by author name");
		System.out.println("5-sort the books based on category");
		System.out.println("6-exit/Quit");

	}

	public static Book getBookDetails() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the book name");
		Book newBook = new Book();
		newBook.setBookName(sc.nextLine());

		System.out.println("enter the categeory of the book");
		newBook.setCategory(sc.nextLine());

		System.out.println("enter the unit price of the book");
		newBook.setUnit_price(sc.nextFloat());
		sc.nextLine();

		System.out.println("enter the author name");
		newBook.setAuthor(sc.nextLine());

		System.out.println("enter the publisher name");
		newBook.setPublisher(sc.nextLine());

		System.out.println("Enter the no of books");
		newBook.setNo_of_books(sc.nextInt());

		return newBook;

	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int choice;
		//boolean isValid = true;
		boolean shouldBreak=false;
		
		LibraryOperations newOperation = new LibraryOperations();

		//while (isValid) {
		
		for(;;) {

			displayMenu();

			System.out.println("enter the choice");

			choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			case 1:
				//System.out.println("Book is added sucessfully with BookId " + newOperation.addBook(getBookDetails()));
				newOperation.getTestData();
				break;
				
			case 2:
				continue;
			case 3:
				System.out.println("Enter the book name to search:");
				System.out.println("The Book is :"+newOperation.searchBook(sc.nextLine()));
				break;
			case 4:
				System.out.println("Enter the Author name :");
				ArrayList<Book> listOfBooksByAuthor = new ArrayList<Book>();
				listOfBooksByAuthor = newOperation.getBooksByAuthor(sc.nextLine());

				System.out.println("the list of books are: ");
				for(Book book:listOfBooksByAuthor) {
					System.out.println(book);
				}
				//System.out.println("the list of books are:"+newOperation.getBooksByAuthor(sc.nextLine()));
				break;
			case 5:
				
				HashMap<String,ArrayList<Book>> MapOfSortedBooksByCategory = new HashMap<String,ArrayList<Book>>();
				
				MapOfSortedBooksByCategory = newOperation.sortBooks();
				
				for(Map.Entry<String, ArrayList<Book>> entry:MapOfSortedBooksByCategory.entrySet()) {
					System.out.println("category "+entry);
				}
				break;

			case 6:

				//isValid = false;
				shouldBreak=true;
			}
			if(shouldBreak) {
				break;
			}
		}
	}

}
