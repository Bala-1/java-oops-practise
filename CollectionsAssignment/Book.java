package CollectionsAssignment;

public class Book {
	private int bookID = 1000;
    private String bookName;
    private String category;
    private float unit_price;
    private String author;
    private String publisher;
    private int no_of_books;
    public Book() {
    }
    public Book(int bookID,String bookName,String category,float unit_price,String author,String publisher,int no_of_books) {
        this.setBookID(bookID);
        this.setBookName(bookName);
        this.setCategory(category);
        this.setUnit_price(unit_price);
        this.setAuthor(author);
        this.setPublisher(publisher);
        this.setNo_of_books(no_of_books);
    }
    public int getBookID() {
        return bookID;
    }
    public  void setBookID(int bookID) {
        this.bookID = bookID;
    }
    public String getBookName() {
        return bookName;
    }
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public float getUnit_price() {
        return unit_price;
    }
    public void setUnit_price(float unit_price) {
        this.unit_price = unit_price;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getPublisher() {
        return publisher;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public int getNo_of_books() {
        return no_of_books;
    }
    public void setNo_of_books(int no_of_books) {
        this.no_of_books = no_of_books;
    }
    @Override
    public String toString() {
        return "[bookID=" + bookID + ", bookName=" + bookName + ", category=" + category + ", unit_price="
                + unit_price + ", author=" + author + ", publisher=" + publisher + ", no_of_books=" + no_of_books+"]";
    }


}
