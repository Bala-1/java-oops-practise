package CollectionsAssignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class LibraryOperations {

	ArrayList<Book> listOfBooks = new ArrayList<Book>();

	public void getTestData() {

		Book book1 = new Book(1001, "Basics of C", "Study material", 200, "greg perry", "modern Library", 200);
		Book book2 = new Book(1002, "Basics of C++", "Study material", 220, "brian W", "SVS publications", 120);
		Book book3 = new Book(1003, "Stories for children", "Story book", 140, "gupta", "modern Library", 20);
		Book book4 = new Book(1004, " Hamlet", "Story book", 900, "shakesphere", "SVS publications", 250);
		Book book5 = new Book(1005, "othello", "Story book", 890, "shakesphere", "modern Library", 210);
		Book book6 = new Book(1006, "Romeo and juliet", "Story book", 2000, "shakesphere", "modern Library", 320);

		listOfBooks.add(book1);
		listOfBooks.add(book2);
		listOfBooks.add(book3);
		listOfBooks.add(book4);
		listOfBooks.add(book5);
		listOfBooks.add(book6);
	}

	/**
	 * This method takes the newbook as argument and add the book to the collection
	 * and return bookid
	 * 
	 **/
	public int addBook(Book newBook) {
		int bookId = 1000;

		for (Book b : listOfBooks) {
			bookId = b.getBookID();
		}
		newBook.setBookID(bookId + 1);
		listOfBooks.add(newBook);

		return newBook.getBookID();
	}

	/**
	 * 
	 * 
	 * **/
	public void updateBook() {

		// need to write code here

	}

	/**
	 * method will take string as argument and search book name in the collection
	 * and return the book if matches.
	 * 
	 **/

	public ArrayList<Book> searchBook(String bookName) {
		Book bookObj = new Book();
		ArrayList<Book> listOfBookDetails = new ArrayList<Book>();

		for (Book book : listOfBooks) {
			if (bookName.equalsIgnoreCase(book.getBookName())) {
				listOfBookDetails.add(book);
			}
		}
		return listOfBookDetails;
	}

	/**
	 * this method will take author name as argument and returns the list of all
	 * books by author name
	 * 
	 **/

	public ArrayList<Book> getBooksByAuthor(String authorName) {

		ArrayList<Book> listOfBooksByAuthor = new ArrayList<Book>();

		for (Book book : listOfBooks) {
			if (authorName.equalsIgnoreCase(book.getAuthor())) {
				listOfBooksByAuthor.add(book);
			}
		}
		return listOfBooksByAuthor;
	}

	/**
	 * 
	 * This method sort the collection based on category and return Map with
	 * category as key and list of books in that category as value in map
	 * 
	 **/

	public HashMap<String, ArrayList<Book>> sortBooks() {

		HashMap<String, ArrayList<Book>> MapOfSortedBooksByCategory = new HashMap<String, ArrayList<Book>>();

		HashSet<String> CategeoriesOfBooks = new HashSet<String>();

		ArrayList<Book> listOfBooksByCategeory = new ArrayList<Book>();
		
		for (Book book : listOfBooks) {
			CategeoriesOfBooks.add(book.getCategory());
		}

		for (String categeory : CategeoriesOfBooks) {
			listOfBooksByCategeory=getListOfBooksByCategory(categeory);
			
			MapOfSortedBooksByCategory.put(categeory, listOfBooksByCategeory);
		}

		return MapOfSortedBooksByCategory;
	}

	public ArrayList<Book> getListOfBooksByCategory(String category) {
		ArrayList<Book> booksByCategory = new ArrayList<Book>();
		for (Book book1 : listOfBooks) {
			if (category.equalsIgnoreCase(book1.getCategory())) {
				booksByCategory.add(book1);
			}
		}
         return booksByCategory;
	}
}
