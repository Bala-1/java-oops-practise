package JavaOopsPractise;

import java.util.Scanner;

public class BankCustomerDetails {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		BankCustomer customer = new BankCustomer("SBI" , -1 ,"Savings");
//		System.out.println(customer);
		
		BankCustomer newBankCustomer = new BankCustomer();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("enter the name of the bank : ");
		newBankCustomer.setBankName(sc.nextLine());
		System.out.println(newBankCustomer.getBankName());
		
		System.out.print("enter the amount going to be deposited : ");
		newBankCustomer.setAmountDeposited(sc.nextInt());
		System.out.println(newBankCustomer.getAmountDeposited());
		sc.nextLine();

		
		System.out.println("enter the type of the account (ie: savings or current deposit) : ");
		newBankCustomer.setAccountType(sc.nextLine());
		System.out.println(newBankCustomer.getAccountType());

		
//		try {
//			System.out.println(newBankCustomer);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			System.out.println(e.getMessage());
//		}
		
		System.out.println(newBankCustomer);



	}

}
