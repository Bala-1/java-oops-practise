package JavaOopsPractise;

public class BankCustomer {
	private String bankName;
	private double amountDeposited;
	private String accountType;

	/**
	 * @param bankName
	 * @param amountDeposited
	 * @param accountType
	 */
	public BankCustomer() {

	}

	public BankCustomer(String bankName, double amountDeposited, String accountType) {
		this.bankName = bankName;
		this.amountDeposited = amountDeposited;
		this.accountType = accountType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public double getAmountDeposited() {
		return amountDeposited;
	}

	public void setAmountDeposited(double amountDeposited) {
		this.amountDeposited = amountDeposited;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double calculateIntrestRate() throws Exception {

		double intrest = 0;
		if (accountType.equals("Savings") || accountType.equals("savings")) {
			if (amountDeposited < 0) {
				throw new Exception("Invalid amount");
			} else if (amountDeposited < 5000000) {
				intrest = amountDeposited * 0.05;
			} else {
				intrest = amountDeposited * 0.055;
			}
		} else if (accountType.equals("Current Deposit") || accountType.equals("current deposit")) {
			if (amountDeposited < 0) {
				throw new Exception("Invalid amount");
			} else if (amountDeposited < 5000000) {
				intrest = amountDeposited * 0.0475;
			} else {
				intrest = amountDeposited * 0.0425;
			}
		}else {
			throw new Exception("Invalid account type");
		}
		return intrest;
	}
	
	public String toString() {
        try {
			return " bankName : " + bankName + "\n amountDeposited : "+ amountDeposited + "\n accountType : "+ accountType + "\n interest : "+ calculateIntrestRate() ;
		} catch (Exception e) {
			return e.getMessage();
		}
    }
}
